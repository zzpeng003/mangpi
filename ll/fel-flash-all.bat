@echo off
set SELFPATH=%~dp0
echo | set /p="Waiting for DFU Device";


%SELFPATH%\bin\sunxi-fel.exe -p uboot output\images\u-boot-sunxi-with-spl.bin
TIMEOUT /T 3 /NOBREAK
%SELFPATH%\bin\dfu-util -a u-boot -D output/images/u-boot-sunxi-with-nand-spl.bin
::@pause

::%SELFPATH%\bin\sunxi-fel.exe -p uboot output\images\u-boot-sunxi-with-spl.bin
TIMEOUT /T 3 /NOBREAK
%SELFPATH%\bin\dfu-util -a dtb -D output/images/devicetree.dtb
::@pause

::%SELFPATH%\bin\sunxi-fel.exe -p uboot output\images\u-boot-sunxi-with-spl.bin
TIMEOUT /T 3 /NOBREAK
%SELFPATH%\bin\dfu-util -a kernel -D output/images/zImage
::@pause

::%SELFPATH%\bin\sunxi-fel.exe -p uboot output\images\u-boot-sunxi-with-spl.bin
TIMEOUT /T 3 /NOBREAK
%SELFPATH%\bin\dfu-util -R -a rom -D output/images/rootfs.ubi